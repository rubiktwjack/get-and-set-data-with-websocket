pragma solidity ^0.5.0;

contract Test {
    uint x = 0;
    event setXEvent(uint _x);
    
    constructor() public {}
    
    function setX(uint _x) public {
        x = _x;
        emit setXEvent(x);
    }
    
    function getX() public view returns(uint) {
        return x;
    }
}
