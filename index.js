"use strict";

let input = $("#input");
let inputButton = $("#inputButton");
let output = $("#output");
let outputButton = $("#outputButton");

// let web3 = new Web3("http://localhost:8545");
let web3 = new Web3(
  new Web3.providers.WebsocketProvider("ws://localhost:8545")
);
let contractInstance = new web3.eth.Contract(dataAbi);

let dataAddress;
var nowAccount;

init();

async function init() {
  let accounts = await web3.eth.getAccounts();

  nowAccount = accounts[9];
  console.log("deploy account : " + nowAccount);

  await contractInstance
    .deploy({
      data: dataBytecode
    })
    .send({
      from: nowAccount,
      gas: 3400000
    })
    .on("receipt", function(receipt) {
      console.log(receipt);
      dataAddress = receipt.contractAddress;
      contractInstance.options.address = dataAddress;
    });
  contractInstance.events.setXEvent().on("data", function(event) {
    console.log(event.returnValues._x);
  });
}

// 當按下input按鍵時
inputButton.on("click", async function() {
  let accounts = await web3.eth.getAccounts();
  nowAccount = accounts[0];
  console.log("input account : " + nowAccount);

  // 輸入
  await contractInstance.methods
    .setX(input.val())
    .send({
      from: nowAccount,
      gas: 3400000
    })
    .on("receipt", function(receipt) {
      console.log(receipt);
    })
    .on("error", function(error) {
      console.log(error);
    });
});

// 當按下output按鍵時
outputButton.on("click", async function() {
  let accounts = await web3.eth.getAccounts();
  nowAccount = accounts[0];
  console.log("output account : " + nowAccount);

  // 讀出
  contractInstance.methods
    .getX()
    .call({ from: nowAccount })
    .then(function(result) {
      console.log(result);
      output.text(result);
    });
});
