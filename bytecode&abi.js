let dataBytecode =
  "60806040526000805534801561001457600080fd5b50610125806100246000396000f3fe6080604052600436106049576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680634018d9aa14604e5780635197c7aa146085575b600080fd5b348015605957600080fd5b50608360048036036020811015606e57600080fd5b810190808035906020019092919050505060ad565b005b348015609057600080fd5b50609760f0565b6040518082815260200191505060405180910390f35b806000819055507f71b492a31a7d95c8f84bc7bd8a4668bbb54d1d6841b6a01e3f91bb984ff00e936000546040518082815260200191505060405180910390a150565b6000805490509056fea165627a7a7230582022fc3c5c82fc315cffaaec4ae7fa3b525e188741a92a74caf55d347afcfb4fb20029";
let dataAbi = [
  {
    constant: false,
    inputs: [
      {
        name: "_x",
        type: "uint256"
      }
    ],
    name: "setX",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: true,
    inputs: [],
    name: "getX",
    outputs: [
      {
        name: "",
        type: "uint256"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    inputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "constructor"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        name: "_x",
        type: "uint256"
      }
    ],
    name: "setXEvent",
    type: "event"
  }
];
